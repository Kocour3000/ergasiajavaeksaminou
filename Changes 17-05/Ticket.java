import java.time.LocalDate;

public class Ticket{

   private int id;
   private int flightId;
   private String ticketType;
   private String seatType;
   private int price;
   private String name;
   private LocalDate issueDate;
   private String mains;
   private String deserts;
   private String drinks;
   private boolean sorted;


   /*
    * Constructors
    */


   public Ticket(int id, int flightId, String ticketType, String seatType, int price, String name, String mains, String deserts, String drinks, LocalDate issueDate){

      setId(id);
      setFlightId(flightId);
      setTicketType(ticketType);
      setSeatType(seatType);
      setPrice(price);
      setName(name);
      setMains(mains);
      setDeserts(deserts);
      setDrinks(drinks);
      setIssueDate(issueDate);
      setSorted(sorted);

   }   // End of constructor


   public Ticket(){

      setName("null");
      setPrice(28008);

   }   // End of constructor


   /*
    * Setters
    */


   public void setId(int id) {
      this.id = id;
   }   // int id Setter

   public void setFlightId(int flightId) {
      this.flightId = flightId;
   }   // int flightId Setter

   public void setTicketType(String ticketType) {
      this.ticketType = ticketType;
   }   // String ticketType Setter

   public void setSeatType(String seatType) {
      this.seatType = seatType;
   }   // String seatType Setter

   public void setPrice(int price) {
      this.price = price;
   }   // int price Setter

   public void setName(String name) {
      this.name = name;
   }   // String name Setter

   public void setIssueDate(LocalDate issueDate) {
      this.issueDate = issueDate;
   }   // String localDate Setter

   public void setMains(String mains){
      this.mains = mains;
   }   // mains Setter

   public void setDeserts(String deserts){
      this.deserts = deserts;
   }   // desert Setter

   public void setDrinks(String drinks){
      this.drinks = drinks;
   }   // drinks Setter

   public void setSorted(boolean sorted){
      this.sorted = sorted;
   }   // sorted Setter


   /*
    * Getters
    */


   public int getId() {
      return this.id;
   }   // getId Getter

   public int getFlightId() {
      return this.flightId;
   }   // getFlightId Getter

   public String getTicketType() {
      return this.ticketType;
   }   // getTicketType Getter

   public String getSeatType() {
      return this.seatType;
   }   // getSeatType Getter

   public int getPrice() {
      return this.price;
   }   // getPrice Getter

   public String getName() {
      return this.name;
   }   // getName Getter

   public LocalDate getIssueDate() {
      return this.issueDate;
   }   // getIssueDate Getter

   public boolean getSorted(){
      return this.sorted;
   }   // sorted Getter


}   // End of class
