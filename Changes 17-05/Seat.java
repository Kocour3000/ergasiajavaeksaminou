abstract class Seat{

   private int id;
   private int row;
   private int column;
   private Ticket ticket;


   /*
    * Constructor
    */


   public Seat(int id, int row, int column, Ticket ticket){

      setId(id);
      setRow(row);
      setColumn(column);
      setTicket(ticket);

   }   // End of constructor {remember.this}


   /*
    * Setters
    */


   public void setId(int id){
      this.id = id;
   }   // id Setter

   public void setRow(int row){
      this.row = row;
   }   // row Setter

   public void setColumn(int column){
      this.column = column;
   }   // column Setter

   public void setTicket(Ticket ticket){
      this.ticket = ticket;
   }   // ticket Setter


   /*
    * Getters
    */


   public int getId(){
      return this.id;
   }   // id Getter

   public int getRow(){
      return this.row;
   }   // row Getter

   public int getColumn(){
      return this.column;
   }   // column Getter

   public Ticket getTicket(){
      return this.ticket;
   }   // ticket Getter


}   // End of class
