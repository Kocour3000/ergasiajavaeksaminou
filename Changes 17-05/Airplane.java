import java.util.Scanner;
public class Airplane{

   private int id;
   private String desc;
   private int rows;
   private int columns;
   private int businessRows;


   /*
    * Constructor
    */


   public Airplane(int id, int rows, int columns, int businessRows){

      setId(id);
      setRows(rows);
      setColumns(columns);
      setBusinessRows(businessRows);

   }   // End of constructor


   /*
    * Setters
    */


   public void setId(int id){
      this.id = id;
   }   // id Setter

   public void setDesc(String desc){
      this.desc = desc;
   }   // desc Setter

   public void setRows(int rows){
      this.rows = rows;
   }   // row Setter

   public void setColumns(int columns){
      this.columns = columns;
   }   // column Setter

   public void setBusinessRows(int businessRows){
      this.businessRows = businessRows;
   }   // businessRows Setter


   /*
    * Getters
    */


   public int getId(){
      return this.id;
   }   // id Getter

   public String getDesc(){
      return this.desc;
   }   // desc Getter

   public int getRows(){
      return this.rows;
   }   // rows Getter

   public int getColumns(){
      return this.columns;
   }   // columns Getter

   public int getBusinessRows(){
      return this.businessRows;
   }   // businessRows Getter


}   // End of class