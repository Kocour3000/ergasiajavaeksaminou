public class BusinessSeat extends Seat{

   private Menu menu;
   private int id;
   private int row;
   private int column;
   private Ticket ticket;


   /*
    * Constructor
    */


   public BusinessSeat(int id, int row, int column, Ticket ticket, Menu menu){

      super(id, row, column, ticket);
      setId(id);
      setRow(row);
      setColumn(column);
      setTicket(ticket);
      setMenu(menu);

   }   // End of constructor {remember.this}


   /*
    * toString
    */


   public String toString(){
   return "id : "+this.id +" Name : "+ticket.getName()+" "+ this.row +" "+ this.column ;
   }   // toString


   /*
    * Setter
    */


   public void setId(int id){
      this.id = id;
   }   // id Setter

   public void setRow(int row){
      this.row = row;
   }   // row Setter

   public void setColumn(int column){
      this.column = column;
   }   // column Setter

   public void setTicket(Ticket ticket){
      this.ticket = ticket;
   }   // ticket Setter

   public void setMenu(Menu menu){
      this.menu = menu;
   }   // menu Setter


   /*
    * Getter
    */


   public int getId(){
      return this.id;
   }   // id Getter

   public int getRow(){
      return this.row;
   }   // row Getter

   public int getColumn(){
      return this.column;
   }   // column Getter

   public Ticket getTicket(){
      return this.ticket;
   }   // ticket Getter

   public Menu getMenu(){
      return this.menu;
   }   // menu Getter


}   // End of class
