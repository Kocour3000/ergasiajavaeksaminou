import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.time.LocalDate;
import java.time.LocalTime; 

public class Client{

   public static void main(String[] args){

   /*
    * Variables
    */

      int exitTrigger = 1;   // used in while
      int inp         = 0;   // used in switch
      int unique      = 0;   // used in Seat creation
      int id;

   /*
    * Arrays
    */

      ArrayList<Airplane> airplaneList = new ArrayList<Airplane>();
      ArrayList<Menu> menuList         = new ArrayList<Menu>();
      ArrayList<Flight> flightList     = new ArrayList<Flight>();
      ArrayList<Ticket> ticketList     = new ArrayList<Ticket>();

   /*
    * Loop
    */

      while ( exitTrigger == 1 ) {

         System.out.println("+-----------------{ Airport Client }-----------------+");
         System.out.println("|                                                    |");
         System.out.println("!   Available options :                              !");
         System.out.println("|                                                    |");
         System.out.println("|        1) to insert new plane                      |");
         System.out.println("!        2) to insert new menu                       !");
         System.out.println("|        3) to insert new flight                     |");
         System.out.println("!        4) to delete a flight                       !");
         System.out.println("|        5) to insert new ticket                     |");
         System.out.println("!        6) to delete a ticket                       !");
         System.out.println("|        7) to edit a passengers menu                |");
         System.out.println("!        8) to return flight availability            !");
         System.out.println("!        0) to exit                                  !");
         System.out.println("|                                                    |");
         System.out.println("+----------------------------------------------------+");
         Scanner scan = new Scanner(System.in);

         inp = checkMenuInput();

   /*
    * Selection
    */

         switch (inp){

   /*
    * Exit
    */

            case 0:
               exitTrigger = 0;
               clearScreen();
               for (int i = 0; i < 50; ++i) //clear console simulation for Eclipse
            	   System.out.println();
               break;

   /*
    * New plane
    */

            case 1:

               id = checkIfItInteger("\n> Enter id : ");

               if ( CheckAirplaneId(id,airplaneList) == 1 ) {
            	  clearScreen();
                  System.out.println("ERROR A01 : There is an airplane with the same id");
                         break;
               } else {
                  int rows = checkIfItInteger("\n> Enter rows : ");
                  int columns = checkIfItInteger("\n> Enter columns : ");
                  int businessRows = checkIfItInteger("\n> Enter business rows : ");

                  airplaneList.add( new Airplane(id, rows, columns, businessRows) );
                  clearScreen();
               }

               break;

   /*
    * New menu
    */

            case 2:

               id = checkIfItInteger("\n> Enter id : ");

               if ( CheckMenuId(id,menuList) == 1 ) {
            	  clearScreen();
                  System.out.println("ERROR A02 : There is a menu with the same id");
                         break;
               } else {

                  ArrayList<String> mains   = new ArrayList<String>();
                  ArrayList<String> deserts = new ArrayList<String>();
                  ArrayList<String> drinks  = new ArrayList<String>();

                     String me1 = "Fill the main courses and enter 0 to exit";
                     String de1 = "Fill the deserts and enter 0 to exit";
                     String dr1 = "Fill the drinks and enter 0 to exit";

                  menuList.add( new Menu(id, ArrayFill(mains,me1), ArrayFill(deserts,de1), ArrayFill(drinks,dr1)));
                  clearScreen();
               }

               break;

   /*
    * New flight
    */

            case 3:

               id = checkIfItInteger("\n> Enter flight id : ");

               if ( CheckFlightId(id,flightList) == 1 ) {
            	  clearScreen();
                  System.out.println("ERROR A03 : There is a flight with the same id");
                         break;
               } else {

               int airplaneId = checkIfItInteger("\n> Enter Airplane id : ");

               if ( getAirplaneFromId(airplaneId,airplaneList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A13 : There is no airplane with this id");
                         break;
               } else {

                  int menuId = checkIfItInteger("\n> Enter menu id : ");

               if ( getMenuFromId(menuId,menuList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A25 : There is no menu with this id");
                         break;
               } else {


                  int e = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getRows();
                  int b = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getBusinessRows();
                  int m = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getColumns();

                  Ticket ticket         = new Ticket();
                  Seat[][] economySeat  = new EconomySeat[e][m];
                  Seat[][] businessSeat = new BusinessSeat[b][m];
                  Menu menu             = menuList.get(getMenuFromId(menuId,menuList));

                  String fromAirport    = checkIfItString("\n> Enter Airport : ");
                  String toAirport      = checkIfItString("\n> Enter destination : ");
                  LocalDate departureDate = checkFlightDate();            
                  LocalTime departureTime = LocalTime.now();

                  flightList.add( new Flight(id, fromAirport, toAirport, airplaneId, menuId, economySeat, businessSeat, departureDate,  departureTime) );

                  clearScreen();
               }   // if else
            }   // if else
         }   //if else

               break;

   /*
    * Remove flight
    */

            case 4:

               id = checkIfItInteger("\n> Enter flight id : ");

               if ( getFlightFromId(id,flightList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A04 : There is no flight with this id");
                         break;
               } else {

                  Flight F              = flightList.get( getFlightFromId(id,flightList) );
                  Seat[][] economySeat  = F.getEconomySeat();
                  Seat[][] businessSeat = F.getBusinessSeat();
                  int airplaneId        = F.getAirplaneId();
                  Airplane A            = airplaneList.get( getAirplaneFromId(airplaneId,airplaneList) );
                  int rows              = A.getRows();
                  int columns           = A.getColumns();
                  int businessRows      = A.getBusinessRows();

                  clearScreen();
                  System.out.println("\n---{Paypal arrow refunds}---\n");

                  for ( int i = 0 ; i < rows ; i++ ){
                     for ( int j = 0 ; j < columns ; j++){
                        if ( economySeat[i][j] instanceof Seat ){
                        String name = economySeat[i][j].getTicket().getName();
                        int price   = economySeat[i][j].getTicket().getPrice();
                        System.out.println(" "+name+" <- "+price+" $ ");
                        }   // if
                     }   // j loop
                  }   // i loop

                  for ( int k = 0 ; k < businessRows ; k++ ){
                     for ( int l =0 ; l < columns ; l++){
                        if ( businessSeat[k][l] instanceof Seat ){
                        String name = businessSeat[k][l].getTicket().getName();
                        int price   = businessSeat[k][l].getTicket().getPrice();
                        System.out.println(" "+name+" <- "+price+" $ ");
                        }   // if
                     }   // j loop
                  }   // i loop
                  System.out.println("\n----------------------------\n");
                  flightList.remove( getFlightFromId(id,flightList) );
               }   // if else
               break;

   /*
    * New ticket
    */

            case 5:

               id = checkIfItInteger("\n> Enter Ticket id : ");

               if ( CheckTicketId(id,ticketList) == 1 ) {
            	  clearScreen();
                  System.out.println("ERROR A05 : There is a ticket with the same id");
                         break;
               } else {

               int flightId = checkIfItInteger("\n> Enter Flight id : ");
               if ( getFlightFromId(flightId,flightList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A15 : There is no flight with this id");
                         break;
               } else {

                  Flight F          = flightList.get( getFlightFromId(flightId,flightList) );
                  String ticketType = selectTicketType();
                  String seatType   = selectSeatType();
                  String name       = checkIfItString("\n> Enter full name : ");
                  LocalDate issueDate = LocalDate.now();
                  
                  int price         = checkIfItInteger("\n> Enter price : ");
                  int menuId        = F.getMenuId();
                  String mains      = "null";
                  String deserts    = "null";
                  String drinks     = "null";

                  ticketList.add( new Ticket(id, flightId, ticketType, seatType, price, name, mains, deserts, drinks , issueDate) );

                  clearScreen();
            }   // if else
         }   // if else
               break;

   /*
    * Remove ticket
    */

            case 6:

               id = checkIfItInteger("\n> Enter ticket id : ");

               if ( getTicketFromId(id,ticketList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A06 : There is no ticket with this id");
                         break;
               } else {
            	  clearScreen();
                  System.out.println("\n---{Paypal arrow refunds}---\n");

                  Ticket T    = ticketList.get( getTicketFromId(id,ticketList) );
                  String name = T.getName();
                  int price   = T.getPrice();

                  System.out.println(" "+name+" <- "+price+" $ ");
                  System.out.println("\n----------------------------\n");
                  ticketList.remove( getTicketFromId(id,ticketList) );
               }

               break;

   /*
    *  Edit food for ticket
    */

       case 7:

               id = checkIfItInteger("\n> Enter ticket id : ");

               if ( getTicketFromId(id,ticketList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A07 : There is no ticket with this id");
                         break;
               } else {

                  if ( !ticketList.get(getTicketFromId(id,ticketList)).getSeatType().equals("business")) {
                	 clearScreen();
                     System.out.println("ERROR A17 : This is not a business seat");
                  } else {
                     Ticket T       = ticketList.get(getTicketFromId(id,ticketList));
                     int flightId   = T.getFlightId();
                     Flight F       = flightList.get(getFlightFromId(flightId,flightList));
                     int menuId     = F.getMenuId();
                     Menu M         = menuList.get(getMenuFromId(menuId,menuList));

                     String mains   = GetStringFromArray(M.getMains());
                     String deserts = GetStringFromArray(M.getDeserts());
                     String drinks  = GetStringFromArray(M.getDrinks());

                     T.setMains(mains);
                     T.setDeserts(deserts);
                     T.setDrinks(drinks);

                     clearScreen();
               }   // if else
            }   // if else
            break;

   /*
    * Show plane
    */

       case 8:

             id = checkIfItInteger("\n> Enter flight id : ");

             if ( getFlightFromId(id,flightList) == -1 ) {
            	clearScreen();
                System.out.println("ERROR A04 : There is no flight with this id");
                break;
             } else {
            	clearScreen();

                Flight F              = flightList.get( getFlightFromId(id,flightList)) ;
                int airplaneId        = F.getAirplaneId();
                Airplane A            = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList));
                Seat[][] economySeat  = F.getEconomySeat();
                Seat[][] businessSeat = F.getBusinessSeat();
                int rows              = A.getRows();
                int columns           = A.getColumns();
                int businessRows      = A.getBusinessRows();

                System.out.print("\n ,;-=====");                                     //
             for ( int k = 0 ; k < columns - 2 ; k++ ){System.out.print("=======");}  // This is
                System.out.print("=====-;,");                                         //  just for
                System.out.print("\n |       ");                                      // the nose
             for ( int k = 0 ; k < columns - 2 ; k++ ){System.out.print("       ");}  //
                System.out.print("       |       Flight id : "+F.getId()+"\n");      //

             for ( int i = 0 ; i < businessRows ; i++ ){
                System.out.print(" |");
                for ( int j = 0 ; j < columns ; j++ ){          //
                   if (businessSeat[i][j] instanceof Seat ){     //
                      System.out.print(" [[x]] ");               //   Accessing
                      } else {                                   //      the
                      System.out.print(" [[ ]] ");               //   business Seat
                   }   // 3 if else                              //      Array
                }   // j loop                                    //
                System.out.print("|\n");                        //
             }   // i loop

             for ( int i = 0 ; i < rows ; i++ ){
                System.out.print(" |");
                for ( int j = 0 ; j < columns ; j++ ){          //
                   if (economySeat[i][j] instanceof Seat){       //
                      System.out.print("  [x]  ");               //   Accessing
                      } else {                                   //      the
                      System.out.print("  [ ]  ");               //   economy Seat
                     }   // if else                              //      Array
                   }   // j loop                                 //
                System.out.print("|\n");                         //
                }   // i loop                                   //

                System.out.print(" |       ");                                       //
             for ( int k = 0 ; k < columns - 2 ; k++ ){System.out.print("       ");}  // This is
                System.out.print("       |>\n");                                      //  just for
                System.out.print(" ^'-=====");                                        // the tail
             for ( int k = 0 ; k < columns - 2 ; k++ ){System.out.print("=======");}  //
                System.out.print("=====-'^\n\n\n");                                  //

             }   // 1 if else
           break;

        }   // End of switch


   /*
    * Automatic seat finder 9000
    */


   for (Flight F : flightList ) {

      int flightId          = F.getId();
      int airplaneId        = F.getAirplaneId();
      int menuId            = F.getMenuId();
      Seat[][] economySeat  = F.getEconomySeat();
      Seat[][] businessSeat = F.getBusinessSeat();
      Airplane A            = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList));
      int rows              = A.getRows();
      int columns           = A.getColumns();
      int businessRows      = A.getBusinessRows();
      Menu menu             = menuList.get( getMenuFromId( menuId, menuList ) );

      for (Ticket T : ticketList ) {
         if(T.getSorted() == false && T.getFlightId() == flightId ){
            if (T.getSeatType().equals("business") ){
outerloopb:
               for ( int i = 0 ; i < businessRows ; i++ ){
                  for ( int j = 0 ; j < columns ; j++ ){
                     if ( businessSeat[i][j] instanceof Seat ){
                        System.out.println("businessSeatExists");
                     } else {
                        T.setSorted(true);
                        businessSeat[i][j] = new BusinessSeat(unique, i, j, T, menu);
                        System.out.println(T.getSeatType()+"businessSeatSet :"+businessSeat[i][j].toString());
                        unique++;
                        break outerloopb;
                            }   // if
                         }   // j loop
                      }   // i loop
                   }   // if business

            if (T.getSeatType().equals("economy") ){
outerloope:
               for ( int i = 0 ; i < rows ; i++ ){
                  for ( int j = 0 ; j < columns ; j++ ){
                     if ( economySeat[i][j] instanceof Seat ){
                        System.out.println("economySeatExists");
                     } else {
                        T.setSorted(true);
                        economySeat[i][j] = new EconomySeat(unique, i, j, T);
                        System.out.println("economySeatSet :"+economySeat[i][j].toString());
                        unique++;
                        break outerloope;
                            }   // if
                         }   // j loop
                      }   // i loop
                   }   // if business
                }   // if sorted and sameflight
             }   // ticket loop
          }   // flight loop
       }   // End of While
    }   // End of main

   
   /*
    * Clear Screen
    */  
   
   
   public static void clearScreen() {
       System.out.print("\033\143\n"); //for terminal
   }
   

   /*
    * Check user input
    */


    public static int checkMenuInput() {
       Scanner sc = new Scanner(System.in);
       int number;
       do {
          System.out.print("[ console ~] : ");
          while (!sc.hasNextInt()) {
             System.out.println("-bash: Not an integer");
             System.out.print("[ console ~] : ");
        sc.next();
          }
          number = sc.nextInt();
         } while (number < 0 || number > 8);
         return number;
    }


    public static int checkIfItInteger(String message) {
       Scanner sc = new Scanner(System.in);
       System.out.print(message);
       int number;
       while (!sc.hasNextInt()) {
          System.out.println("-bash: Not an integer");
          System.out.print(message);
          sc.next();   }
       number = sc.nextInt();
       return number;
    }


   public static String checkIfItString(String message){
      Scanner sc = new Scanner(System.in);
      System.out.print(message);
      String string;
      string = sc.nextLine();
      return string;
   }

   
   /*
    * check flight date
    */  
   
   
   public static LocalDate checkFlightDate(){
		Scanner sc = new Scanner(System.in);
		int year, month, day;
		System.out.println("Enter date (year) : ");
		year = sc.nextInt();

		do {
			System.out.println("Enter date (month) : ");	
			while (!sc.hasNextInt()) {
				System.out.println("-bash: Not an integer");
				System.out.print("[ console ~] : ");
				sc.next();
			}
			month = sc.nextInt();
			if (month < 1 || month > 12)
				System.out.println("-bash: Incorrect number");	
		} while (month < 1 || month > 12);	
		do {
			System.out.println("Enter date (day) : ");		
			while (!sc.hasNextInt()) {
				System.out.println("-bash: Not an integer");
				System.out.print("[ console ~] : ");
				sc.next();
			}
			day = sc.nextInt();
			if (day < 1 || day > 31)
				System.out.println("-bash: Incorrect number");	
		} while (day < 1 || day > 31);		
		LocalDate date = LocalDate.of(year, month, day);
		return date;	   
   }

   /*
    * checkId
    */


   public static int CheckAirplaneId(int id , ArrayList<Airplane> airplaneList){
      for (Airplane j : airplaneList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   public static int CheckMenuId(int id , ArrayList<Menu> menuList){
      for (Menu j : menuList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   public static int CheckFlightId(int id , ArrayList<Flight> flightList){
      for (Flight j : flightList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   public static int CheckTicketId(int id , ArrayList<Ticket> ticketList){
      for (Ticket j : ticketList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   /*                 This does **NOT** return an object
    * Get<>FromId           Only an ArrayList index
    */


   public static int getAirplaneFromId(int id , ArrayList<Airplane> airplaneList){
      for (int k = 0; k < airplaneList.size(); k ++ ) {
         if (airplaneList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getAirplaneFromId


   public static int getFlightFromId(int id , ArrayList<Flight> flightList){
      for (int k = 0; k < flightList.size(); k ++ ) {
         if (flightList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getFlightFromId


   public static int getMenuFromId(int id , ArrayList<Menu> menuList){
      for (int k = 0; k < menuList.size(); k ++ ) {
         if (menuList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getMenuFromId


   public static int getTicketFromId(int id , ArrayList<Ticket> ticketList){
      for (int k = 0; k < ticketList.size(); k ++ ) {
         if (ticketList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getMenuFromId


   /*
    * FillArrayUntil0
    */


   public static ArrayList<String> ArrayFill(ArrayList<String> arrayList, String message){
      Scanner scan = new Scanner(System.in);
      String z = "Not0";
      int counter = 1;
      System.out.println(message);
      while ( true ){
         System.out.print("\nEnter ["+counter+"] :");
         z = scan.nextLine();
         if ( z.equals("0") ){break;}
         arrayList.add(z);
         System.out.println(z);
         counter++;
      }
      return arrayList;
   }   // End of FillArrayUntil0


   /*
    * GetArrayFromString
    */


   public static String GetStringFromArray(ArrayList<String> arrayList){
      Scanner scan = new Scanner(System.in);
      for ( int i = 0 ; i < arrayList.size() ; i++ ){
         System.out.println("["+i+"] ->  "+arrayList.get(i));
      }
      int z = checkIfItInteger("Enter selection number : ");
      return arrayList.get(z);
   }   // GetStringFromArray


   /*
    * selectSeatType
    */


   public static String selectSeatType(){
     int sel = checkIfItInteger("\n> [1]-Economy [2]-Business : ");
     if( sel == 2 ) { return "business"; }
     return "economy";
   }   // End of SelectSeatType


   public static String selectTicketType(){
     int sel = checkIfItInteger("\n> [1]-Discounted [2]-Standard : ");
     if( sel == 2 ) { return "standard"; }
     return "discounted";
   }  // End of selectTicketType


}   // End of Client class