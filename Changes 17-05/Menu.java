import java.util.ArrayList;
import java.util.List;
public class Menu{

   private int id;
   private ArrayList<String> mains;
   private ArrayList<String> deserts;
   private ArrayList<String> drinks;


   /*
    * Constructor
    */


   public Menu(int id, ArrayList<String> mains, ArrayList<String> deserts, ArrayList<String> drinks){

      setId(id);
      setMains(mains);
      setDeserts(deserts);
      setDrinks(drinks);

   }   // End of constructor {remember.this}


   /*
    * Setters
    */


   public void setId(int id){
      this.id = id;
   }   // id Setter

   public void setMains(ArrayList<String> mains){
      this.mains = mains;
   }   // mains Setter

   public void setDrinks(ArrayList<String> drinks){
      this.drinks = drinks;
   }   // drinks Setter

   public void setDeserts(ArrayList<String> deserts){
      this.deserts = deserts;
   }   // desert Setter


   /*
    * Getters
    */


   public int getId(){
      return this.id;
   }   // id Getter

   public ArrayList<String> getMains(){
      return this.mains;
   }   // mains Getter

   public ArrayList<String> getDeserts(){
      return this.deserts;
   }   // mains Getter

   public ArrayList<String> getDrinks(){
      return this.drinks;
   }   // mains Getter


}   // End of class
