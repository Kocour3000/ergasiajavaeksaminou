import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Client{

   public static void main(String[] args){

   /*
    * Variables
    */

      int i = 1;   // used in while
      int inp = 0;   // used in switch
      int id;

   /*
    * Arrays
    */

      ArrayList<Airplane> airplaneList  = new ArrayList<Airplane>();
      ArrayList<Menu> menuList  = new ArrayList<Menu>();
      ArrayList<Flight> flightList  = new ArrayList<Flight>();
      ArrayList<Ticket> ticketList  = new ArrayList<Ticket>();

   /*
    * Loop
    */


      // airplaneList.add( new Airplane(123,123,123,123) );
      // airplaneList.add( new Airplane(124,123,123,123) );
      // flightList.add( new Flight(12,"athens","nirger",123,12,23));
                //
      // for (Airplane j : airplaneList ) {
      //    System.out.println(j.getId()); }
      // for (int l = 0; l < airplaneList.size(); l ++ ) {
      // System.out.println(airplaneList.get(l).getId()); }



      while ( i == 1 ) {

         System.out.println("+-----------------{ Airport Client }-----------------+");
         System.out.println("|                                                    |");
         System.out.println("!   Available options :                              !");
         System.out.println("|                                                    |");
         System.out.println("|        1) to insert new plane                      |");
         System.out.println("!        2) to insert new menu                       !");
         System.out.println("|        3) to insert new flight                     |");
         System.out.println("!        4) to delete a flight                       !");
         System.out.println("|        5) to insert new ticket                     |");
         System.out.println("!        6) to delete a ticket                       !");
         System.out.println("|        7) to edit a passengers menu                |");
         System.out.println("!        8) to return flight availability            !");
         System.out.println("!        0) to exit                                  !");
         System.out.println("|                                                    |");
         System.out.println("+----------------------------------------------------+");

         Scanner scan = new Scanner(System.in);
         inp = checkMenuInput();
         switch (inp){
            case 0:
               i = 0;
               System.out.print("\033\143");
               break;
            case 1:

               System.out.print("\n> Enter id : ");
               id = checkIfItInteger();

               if ( CheckAirplaneId(id,airplaneList) == 1 ) {
                  System.out.print("\033\143");
                  System.out.println("ERROR A01 : There is an airplane with the same id");
                         break;
               } else {
                  System.out.print("\n> Enter rows : ");
                  int rows = checkIfItInteger();
                  System.out.print("\n> Enter columns : ");
                  int columns = checkIfItInteger();
                  System.out.print("\n> Enter business rows : ");
                  int businessRows = checkIfItInteger();

                  airplaneList.add( new Airplane(id, rows, columns, businessRows) );
                  System.out.print("\033\143");
               }

               break;
            case 2:

               System.out.print("\n> Enter id : ");
               id = checkIfItInteger();
               scan.nextLine();   // used to consume nl leftover

               if ( CheckMenuId(id,menuList) == 1 ) {
                  System.out.print("\033\143");
                  System.out.println("ERROR A02 : There is a menu with the same id");
                         break;
               } else {

                  ArrayList<String> mains = new ArrayList<String>();
                  ArrayList<String> deserts = new ArrayList<String>();
                  ArrayList<String> drinks= new ArrayList<String>();

                  String me1 = "Fill the main courses and enter 0 to exit";
                  String de1 = "Fill the deserts and enter 0 to exit";
                  String dr1 = "Fill the drinks and enter 0 to exit";

                  menuList.add( new Menu(id, ArrayFill(mains,me1), ArrayFill(deserts,de1), ArrayFill(drinks,dr1)));
                  System.out.print("\033\143");
               }

               break;
            case 3:

               System.out.print("\n> Enter flight id : ");
               id = checkIfItInteger();

               if ( CheckFlightId(id,flightList) == 1 ) {
                  System.out.print("\033\143");
                  System.out.println("ERROR A03 : There is a flight with the same id");
                         break;
               } else {

               System.out.print("\n> Enter Airplane id : ");
               int airplaneId = checkIfItInteger();
               scan.nextLine();   // used to consume nl leftover
               if ( getAirplaneFromId(airplaneId,airplaneList) == -1 ) {
                  System.out.print("\033\143");
                  System.out.println("ERROR A13 : There is no airplane with this id");
                         break;
               } else {

                  int n = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getRows();
                  int m = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getColums();

                  System.out.print("\n> Enter Airport : ");
                  String fromAirport = scan.nextLine();
                  System.out.print("\n> Enter destination : ");
                  String toAirport = scan.nextLine();

                  flightList.add( new Flight(id, fromAirport, toAirport, airplaneId,n ,m) );
                  System.out.print("\033\143");
               }}

               break;
            case 4:

               System.out.print("\n> Enter flight id : ");
               id = checkIfItInteger();

               if ( getFlightFromId(id,flightList) == -1 ) {
                  System.out.print("\033\143");
                  System.out.println("ERROR A04 : There is no flight with this id");
                         break;
               } else {
                  flightList.remove( getFlightFromId(id,flightList) );
                  System.out.print("\033\143");
               }

               break;
            case 5:


               System.out.print("\n> Enter Ticket id : ");
               id = checkIfItInteger();

               if ( CheckTicketId(id,ticketList) == 1 ) {
                  System.out.print("\033\143");
                  System.out.println("ERROR A05 : There is a ticket with the same id");
                         break;
               } else {

               System.out.print("\n> Enter Flight id : ");
               int flightId = checkIfItInteger();
               scan.nextLine();   // used to consume nl leftover
               if ( getFlightFromId(flightId,flightList) == -1 ) {
                  System.out.print("\033\143");
                  System.out.println("ERROR A15 : There is no flight with this id");
                         break;
               } else {


                  System.out.print("\n> Enter type of ticket : ");
                  String ticketType = scan.nextLine();
                  System.out.print("\n> Enter type of seat : ");
                  String seatType = scan.nextLine();
                  System.out.print("\n> Enter full name : ");
                  String name = scan.nextLine();
                  System.out.print("\n> Enter date : ");
                  String localDate = scan.nextLine();
                  System.out.print("\n> Enter price : ");
                  int price = scan.nextInt();
                  System.out.print("\n> Enter menu id : ");
                  int menuId = scan.nextInt();


               if ( getMenuFromId(menuId,menuList) == -1 ) {
                  System.out.print("\033\143");
                  System.out.println("ERROR A25 : There is no menu with this id");
                         break;
               } else {

                  String mains = "";
                  String deserts = "";
                  String drinks = "";

                  ticketList.add( new Ticket(id, flightId, ticketType, seatType, price, name, mains, deserts, drinks , localDate) );
                  System.out.print("\033\143");
               }}}


               break;
            case 6:


               System.out.print("\n> Enter ticket id : ");
               id = checkIfItInteger();

               if ( getTicketFromId(id,ticketList) == -1 ) {
                  System.out.print("\033\143");
                  System.out.println("ERROR A06 : There is no ticket with this id");
                         break;
               } else {
                  ticketList.remove( getTicketFromId(id,ticketList) );
                  System.out.print("\033\143");
               }


               break;
            case 7:


               System.out.print("\n> Enter ticket id : ");
               id = checkIfItInteger();

               if ( getTicketFromId(id,ticketList) == -1 ) {
                  System.out.print("\033\143");
                  System.out.println("ERROR A06 : There is no ticket with this id");
                         break;
               } else {





                  ticketList.get(getTicketFromId(id,ticketList)).setMains("food");
                  ticketList.get(getTicketFromId(id,ticketList)).setDeserts("food");
                  ticketList.get(getTicketFromId(id,ticketList)).setDrinks("water");
                  System.out.print("\033\143");
               }


               break;
            case 8:
               break;

         }   // End of switch

      }   // End of While

   }   // End of main

   
   
   /*
    * checkUserInput
    */   
   
   
	public static int checkMenuInput() {
		Scanner sc = new Scanner(System.in);
		int number;
		do {
		    System.out.println("Please enter a positive number from 0 to 8!");
		    while (!sc.hasNextInt()) {
		        System.out.println("That's not a number!");
		        sc.next();
		    }
		    number = sc.nextInt();
		} while (number < 0 || number > 8);    
		return number;
	}
   
	public static int checkIfItInteger() {
		Scanner sc = new Scanner(System.in);
		int number;
		while (!sc.hasNextInt()) {
		    System.out.println("That's not a number!");
		    sc.next();
		}
		number = sc.nextInt();
		return number;
	}

   /*
    * checkId
    */


   public static int CheckAirplaneId(int id , ArrayList<Airplane> airplaneList){
      for (Airplane j : airplaneList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   public static int CheckMenuId(int id , ArrayList<Menu> menuList){
      for (Menu j : menuList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   public static int CheckFlightId(int id , ArrayList<Flight> flightList){
      for (Flight j : flightList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   public static int CheckTicketId(int id , ArrayList<Ticket> ticketList){
      for (Ticket j : ticketList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   /*
    * Get<>FromId
    */


   public static int getAirplaneFromId(int id , ArrayList<Airplane> airplaneList){
      for (int k = 0; k < airplaneList.size(); k ++ ) {
         if (airplaneList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getAirplaneFromId


   public static int getFlightFromId(int id , ArrayList<Flight> flightList){
      for (int k = 0; k < flightList.size(); k ++ ) {
         if (flightList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getFlightFromId


   public static int getMenuFromId(int id , ArrayList<Menu> menuList){
      for (int k = 0; k < menuList.size(); k ++ ) {
         if (menuList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getMenuFromId


   public static int getTicketFromId(int id , ArrayList<Ticket> ticketList){
      for (int k = 0; k < ticketList.size(); k ++ ) {
         if (ticketList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getMenuFromId


   /*
    * FillArrayUntil0
    */


   public static ArrayList<String> ArrayFill(ArrayList<String> arrayList, String message){
      Scanner scan = new Scanner(System.in);
      String z = "Not0";
      int counter = 1;
      System.out.println(message);
      while ( true ){
         System.out.print("\nEnter ["+counter+"] :");
         z = scan.nextLine();
         if ( z.equals("0") ){break;}
         arrayList.add(z);
         System.out.println(z);
         counter++;
      }
      return arrayList;
   }   // End of FillArrayUntil0


   public static String GetStringFromArray(ArrayList<String> arrayList){
      Scanner scan = new Scanner(System.in);
      for ( int i = 0 ; i < arrayList.size() ; i++ ){

         System.out.println("["+i+"] ->  "+arrayList.get(i));
      }
      System.out.println("Enter selection number :");
      int z = scan.nextInt();

      return arrayList.get(z);
   }   //


}   // End of class