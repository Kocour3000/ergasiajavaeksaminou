public class Menu{

	private int id;
	private String mains;
	private String deserts;
	private String drinks;


	public Menu(int id, String mains, String deserts, String drinks){

		setId(id);
		setMains(mains);
		setDeserts(deserts);
		setDrinks(drinks);

	}	// End of constractor {remember.this}


	/*
	 * Setters
	 */


	public void setId(int id){
		this.id = id;
	}	// id Setter

	public void setMains(String Mains){
		this.mains = mains;
	}	// mains Setter

	public void setDrinks(String drinks){
		this.drinks = drinks;
	}	// drinks Setter

	public void setDeserts(String Deserts){
		this.deserts = deserts;
	}	// desert Setter


	/*
	 * Getters
	 */


	public int getId(){
		return this.id;
	}	// id Getter
	//<-





}	// End of class
//<-
