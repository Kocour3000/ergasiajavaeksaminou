public class Ticket{

	int id;
	int flightId;
	String ticketType;
	String seatType;
	int price;
	String name;
	Menu menu;
	String localDate;
	
	public Ticket(int id, int flightId, String ticketType, String seatType, int price, String name, Menu menu, String localDate){

		this.id = id;
		this.flightId = flightId;
		this.ticketType = ticketType;
		this.seatType = seatType;
		this.price = price;
		this.name = name;
		this.menu = menu;

	}	// End of constructor
	
	public void setId (int id) {
		this.id = id;
	}
	
	public int getId () {
		return this.id;
	}
	
	public void setflightId (int flightId) {
		this.flightId = flightId;
	}
	
	public int getflightId () {
		return this.flightId;
	}
	
	public void setTicketType (String ticketType) {
		this.ticketType = ticketType;
	}
	
	public String getTicketType () {
		return this.ticketType;
	}
	
	public void setSeatType (String seatType) {
		this.seatType = seatType;
	}
	
	public String getSeatType () {
		return this.seatType;
	}	
	
	public void setPrice (int price) {
		this.price = price;
	}
	
	public int getPrice () {
		return this.price;
	}	
	
	public void setName (String name) {
		this.name = name;
	}
	
	public String getName () {
		return this.name;
	}
	
	public void setMenu (Menu menu) {
		this.menu = menu;
	}
	
	public Menu getMenu () {
		return this.menu;
	}
	
	public void setLocalDate (String localDate) {
		this.localDate = localDate;
	}
	
	public String getLocalDate () {
		return this.localDate;
	}
	
}	// End of class
