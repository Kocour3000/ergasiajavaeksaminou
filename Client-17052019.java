import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Client{

   public static void main(String[] args){

   /*
    * Variables
    */

      int exitTrigger = 1;   // used in while
      int inp = 0;   // used in switch
      int unique = 0;   // used in Seat creation
      int id;

   /*
    * Arrays
    */

      ArrayList<Airplane> airplaneList  = new ArrayList<Airplane>();
      ArrayList<Menu> menuList  = new ArrayList<Menu>();
      ArrayList<Flight> flightList  = new ArrayList<Flight>();
      ArrayList<Ticket> ticketList  = new ArrayList<Ticket>();

   /*
    * Loop
    */


      while ( exitTrigger == 1 ) {

         System.out.println("+-----------------{ Airport Client }-----------------+");
         System.out.println("|                                                    |");
         System.out.println("!   Available options :                              !");
         System.out.println("|                                                    |");
         System.out.println("|        1) to insert new plane                      |");
         System.out.println("!        2) to insert new menu                       !");
         System.out.println("|        3) to insert new flight                     |");
         System.out.println("!        4) to delete a flight                       !");
         System.out.println("|        5) to insert new ticket                     |");
         System.out.println("!        6) to delete a ticket                       !");
         System.out.println("|        7) to edit a passengers menu                |");
         System.out.println("!        8) to return flight availability            !");
         System.out.println("!        0) to exit                                  !");
         System.out.println("|                                                    |");
         System.out.println("+----------------------------------------------------+");
         Scanner scan = new Scanner(System.in);

         inp = checkMenuInput();

   /*
    * Selection
    */

         switch (inp){

   /*
    * Exit
    */

            case 0:
               exitTrigger = 0;
               clearScreen();
               break;

   /*
    * New plane
    */

            case 1:

               id = checkIfItInteger("\n> Enter id : ");

               if ( CheckAirplaneId(id,airplaneList) == 1 ) {
            	  clearScreen();
                  System.out.println("ERROR A01 : There is an airplane with the same id");
                         break;
               } else {
                  int rows = checkIfItInteger("\n> Enter rows : ");
                  int columns = checkIfItInteger("\n> Enter columns : ");
                  int businessRows = checkIfItInteger("\n> Enter business rows : ");

                  airplaneList.add( new Airplane(id, rows, columns, businessRows) );
                  clearScreen();
               }

               break;

   /*
    * New menu
    */

            case 2:

               id = checkIfItInteger("\n> Enter id : ");

               if ( CheckMenuId(id,menuList) == 1 ) {
            	  clearScreen();
                  System.out.println("ERROR A02 : There is a menu with the same id");
                         break;
               } else {

                  ArrayList<String> mains = new ArrayList<String>();
                  ArrayList<String> deserts = new ArrayList<String>();
                  ArrayList<String> drinks= new ArrayList<String>();

                     String me1 = "Fill the main courses and enter 0 to exit";
                     String de1 = "Fill the deserts and enter 0 to exit";
                     String dr1 = "Fill the drinks and enter 0 to exit";

                  menuList.add( new Menu(id, ArrayFill(mains,me1), ArrayFill(deserts,de1), ArrayFill(drinks,dr1)));
                  clearScreen();
               }

               break;

   /*
    * New flight
    */

            case 3:

               id = checkIfItInteger("\n> Enter flight id : ");

               if ( CheckFlightId(id,flightList) == 1 ) {
            	  clearScreen();
                  System.out.println("ERROR A03 : There is a flight with the same id");
                         break;
               } else {

               int airplaneId = checkIfItInteger("\n> Enter Airplane id : ");

               if ( getAirplaneFromId(airplaneId,airplaneList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A13 : There is no airplane with this id");
                         break;
               } else {

                  int menuId = checkIfItInteger("\n> Enter menu id : ");

               if ( getMenuFromId(menuId,menuList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A25 : There is no menu with this id");
                         break;
               } else {


                  int e = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getRows();
                  int b = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getBusinessRows();
                  int m = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getColums();

                  Ticket ticket = new Ticket();
                  Seat[][] economySeat = new EconomySeat[e][m];
                  Seat[][] businessSeat = new BusinessSeat[b][m];
                  Menu menu = menuList.get(getMenuFromId(menuId,menuList));

                  String fromAirport = checkIfItString("\n> Enter Airport : ");
                  String toAirport = checkIfItString("\n> Enter destination : ");

                  flightList.add( new Flight(id, fromAirport, toAirport, airplaneId, menuId, economySeat, businessSeat) );

                  clearScreen();
               }   // if else
            }   // if else
         }   //if else

               break;

   /*
    * Remove flight
    */

            case 4:

               id = checkIfItInteger("\n> Enter flight id : ");

               if ( getFlightFromId(id,flightList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A04 : There is no flight with this id");
                         break;
               } else {
                  flightList.remove( getFlightFromId(id,flightList) );
               }   // if else

               break;

   /*
    * New ticket
    */

            case 5:

               id = checkIfItInteger("\n> Enter Ticket id : ");

               if ( CheckTicketId(id,ticketList) == 1 ) {
            	  clearScreen();
                  System.out.println("ERROR A05 : There is a ticket with the same id");
                         break;
               } else {

               int flightId = checkIfItInteger("\n> Enter Flight id : ");
               if ( getFlightFromId(flightId,flightList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A15 : There is no flight with this id");
                         break;
               } else {

                  String ticketType = selectTicketType();
                  String seatType = selectSeatType();

                  String name = checkIfItString("\n> Enter full name : ");
                  String localDate = checkIfItString("\n> Enter date : ");

                  int price = checkIfItInteger("\n> Enter price : ");
                  int menuId = checkIfItInteger("\n> Enter menu id : ");


               if ( getMenuFromId(menuId,menuList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A25 : There is no menu with this id");
                         break;
               } else {

                  String mains = "";
                  String deserts = "";
                  String drinks = "";

                  ticketList.add( new Ticket(id, flightId, ticketType, seatType, price, name, mains, deserts, drinks , localDate) );

                  clearScreen();
               }   // if else
            }   // if else
         }   // if else
               break;

   /*
    * Remove ticket
    */

            case 6:

               id = checkIfItInteger("\n> Enter ticket id : ");

               if ( getTicketFromId(id,ticketList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A06 : There is no ticket with this id");
                         break;
               } else {
                  ticketList.remove( getTicketFromId(id,ticketList) );
                  clearScreen();
               }

               break;

   /*
    *  Edit food for ticket
    */

       case 7:

               id = checkIfItInteger("\n> Enter ticket id : ");

               if ( getTicketFromId(id,ticketList) == -1 ) {
            	  clearScreen();
                  System.out.println("ERROR A07 : There is no ticket with this id");
                         break;
               } else {

                  if ( !ticketList.get(getTicketFromId(id,ticketList)).getSeatType().equals("business")) {
                	 clearScreen();
                     System.out.println("ERROR A17 : This is not a business seat");
                  } else {
                     int flightId =  ticketList.get(getTicketFromId(id,ticketList)).getFlightId();
                     int menuId = flightList.get(getFlightFromId(flightId,flightList)).getMenuId();
                     String mains = GetStringFromArray(menuList.get(getMenuFromId(menuId,menuList)).getMains());
                     String deserts = GetStringFromArray(menuList.get(getMenuFromId(menuId,menuList)).getDeserts());
                     String drinks = GetStringFromArray(menuList.get(getMenuFromId(menuId,menuList)).getDrinks());

                     ticketList.get(getTicketFromId(id,ticketList)).setMains(mains);
                     ticketList.get(getTicketFromId(id,ticketList)).setDeserts(deserts);
                     ticketList.get(getTicketFromId(id,ticketList)).setDrinks(drinks);

                     clearScreen();
               }   // if else
            }   // if else
            break;

   /*
    * Show plane
    */

       case 8:

             id = checkIfItInteger("\n> Enter flight id : ");

             if ( getFlightFromId(id,flightList) == -1 ) {
            	clearScreen();
                System.out.println("ERROR A04 : There is no flight with this id");
                break;
             } else {
            	clearScreen();

                Flight F = flightList.get( getFlightFromId(id,flightList)) ;
                int airplaneId = F.getAirplaneId();

                Airplane A = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList));
                Seat[][] economySeat = F.getEconomySeat();
                Seat[][] businessSeat = F.getBusinessSeat();

                int rows = A.getRows();
                int columns = A.getColums();
                int businessRows = A.getBusinessRows();

             for ( int i = 0 ; i < businessRows ; i++ ){
                for ( int j = 0 ; j < columns ; j++ ){           //
                   if (businessSeat[i][j] instanceof Seat ){     //
                      System.out.print("[[x]]  ");               //   Accesing
                      } else {                                   //      the
                      System.out.print("[[ ]]  ");               //   businessSeat
                   }   // 3 if else                              //      Array
                }   // j loop                                    //
                System.out.print("\n");                          //
             }   // i loop

             for ( int i = 0 ; i < rows ; i++ ){
                for ( int j = 0 ; j < columns ; j++ ){           //
                   if (economySeat[i][j] instanceof Seat){       //
                      System.out.print(" [x]   ");               //   Accesing
                      } else {                                   //      the
                      System.out.print(" [ ]   ");               //   economySeat
                     }   // if else                              //      Array
                   }   // j loop                                 //
                System.out.print("\n");                          //
                }   // i loop                                    //
             }   // 1 if else

           break;

        }   // End of switch


   /*
    * Automatic seat finder 9000
    */


   for (Flight F : flightList ) {

      int flightId = F.getId();
      int airplaneId = F.getAirplaneId();
      int menuId = F.getMenuId();
      int rows = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getRows();
      int columns = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getColums();
      int businessRows = airplaneList.get(getAirplaneFromId(airplaneId,airplaneList)).getBusinessRows();

      Seat[][] economySeat = F.getEconomySeat();
      Seat[][] businessSeat = F.getBusinessSeat();
      Menu menu = menuList.get( getMenuFromId( menuId, menuList ) );

      for (Ticket T : ticketList ) {
         if(T.getSorted() == false && T.getFlightId() == flightId ){
            if (T.getSeatType().equals("business") ){
outerloopb:
               for ( int i = 0 ; i < businessRows ; i++ ){
                  for ( int j = 0 ; j < columns ; j++ ){
                     if ( businessSeat[i][j] instanceof Seat ){
                        System.out.println("businessSeatExists");
                     } else {
                        T.setSorted(true);
                        businessSeat[i][j] = new BusinessSeat(unique, i, j, T, menu);
                        System.out.println(T.getSeatType()+"businessSeatSet :"+businessSeat[i][j].toString());
                        unique++;
                        break outerloopb;
                            }   // if
                         }   // j loop
                      }   // i loop
                   }   // if business

            if (T.getSeatType().equals("economy") ){
outerloope:
               for ( int i = 0 ; i < rows ; i++ ){
                  for ( int j = 0 ; j < columns ; j++ ){
                     if ( economySeat[i][j] instanceof Seat ){
                        System.out.println("economySeatExists");
                     } else {
                        T.setSorted(true);
                        economySeat[i][j] = new EconomySeat(unique, i, j, T);
                        System.out.println("economySeatSet :"+economySeat[i][j].toString());
                        unique++;
                        break outerloope;
                            }   // if
                         }   // j loop
                      }   // i loop
                   }   // if business
                }   // if sorted and sameflight
             }   // ticket loop
          }   // flight loop
       }   // End of While
    }   // End of main

   
   /*
    * ClearScreen
    */  
   
   
   public static void clearScreen() {
       System.out.print("\033\143\n");
   }
   

   /*
    * Check user input
    */


    public static int checkMenuInput() {
       Scanner sc = new Scanner(System.in);
       int number;
       do {
          System.out.print("[ console ~] : ");
          while (!sc.hasNextInt()) {
             System.out.println("-bash: Not an integer");
             System.out.print("[ console ~] : ");
        sc.next();
          }
          number = sc.nextInt();
         } while (number < 0 || number > 8);
         return number;
    }


    public static int checkIfItInteger(String message) {
       Scanner sc = new Scanner(System.in);
       System.out.print(message);
       int number;
       while (!sc.hasNextInt()) {
          System.out.println("-bash: Not an integer");
          System.out.print(message);
          sc.next();   }
       number = sc.nextInt();
       return number;
    }


   public static String checkIfItString(String message){
      Scanner sc = new Scanner(System.in);
      System.out.print(message);
      String string;
      string = sc.nextLine();
      return string;
   }


   /*
    * checkId
    */


   public static int CheckAirplaneId(int id , ArrayList<Airplane> airplaneList){
      for (Airplane j : airplaneList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   public static int CheckMenuId(int id , ArrayList<Menu> menuList){
      for (Menu j : menuList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   public static int CheckFlightId(int id , ArrayList<Flight> flightList){
      for (Flight j : flightList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   public static int CheckTicketId(int id , ArrayList<Ticket> ticketList){
      for (Ticket j : ticketList ) {
         if (j.getId() == id ) { return 1; }}
      return 0;
   }   // End of checkId


   /*
    * Get<>FromId
    */


   public static int getAirplaneFromId(int id , ArrayList<Airplane> airplaneList){
      for (int k = 0; k < airplaneList.size(); k ++ ) {
         if (airplaneList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getAirplaneFromId


   public static int getFlightFromId(int id , ArrayList<Flight> flightList){
      for (int k = 0; k < flightList.size(); k ++ ) {
         if (flightList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getFlightFromId


   public static int getMenuFromId(int id , ArrayList<Menu> menuList){
      for (int k = 0; k < menuList.size(); k ++ ) {
         if (menuList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getMenuFromId


   public static int getTicketFromId(int id , ArrayList<Ticket> ticketList){
      for (int k = 0; k < ticketList.size(); k ++ ) {
         if (ticketList.get(k).getId() == id ) { return k; }}
      return -1;
   }   // End of getMenuFromId


   /*
    * FillArrayUntil0
    */


   public static ArrayList<String> ArrayFill(ArrayList<String> arrayList, String message){
      Scanner scan = new Scanner(System.in);
      String z = "Not0";
      int counter = 1;
      System.out.println(message);
      while ( true ){
         System.out.print("\nEnter ["+counter+"] :");
         z = scan.nextLine();
         if ( z.equals("0") ){break;}
         arrayList.add(z);
         System.out.println(z);
         counter++;
      }
      return arrayList;
   }   // End of FillArrayUntil0


   /*
    * GetArrayFromString
    */


   public static String GetStringFromArray(ArrayList<String> arrayList){
      Scanner scan = new Scanner(System.in);
      for ( int i = 0 ; i < arrayList.size() ; i++ ){

         System.out.println("["+i+"] ->  "+arrayList.get(i));
      }
      int z = checkIfItInteger("Enter selection number : ");

      return arrayList.get(z);
   }   // GetStringFromArray


   /*
    * selectSeatType
    */


   public static String selectSeatType(){
     int sel = checkIfItInteger("\n> [1]-Economy [2]-Business : ");
     if( sel == 2 ) { return "business"; }
     return "economy";
   }   // End of SelectSeatType


   public static String selectTicketType(){
     int sel = checkIfItInteger("\n> [1]-Discounted [2]-Standard : ");
     if( sel == 2 ) { return "standard"; }
     return "discounted";
   }  // End of selectTicketType


}   // End of Client class